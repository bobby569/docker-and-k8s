### Notes

To build the images and run the container use docker for dev:

    docker build -f Dockerfile.dev .
    docker run -it --rm -p 3000:3000 -v /app/node_modules -v $(pwd):/app <image id>

To run the container with docker-compose for dev:

    docker-compose up

To build the images and hosted on nginx:

    docker build .

To run the container with nginx:

    docker run -it --rm -p 8080:80 <image id>
