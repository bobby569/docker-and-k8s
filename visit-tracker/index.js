const app = require('express')();
const client = require('redis').createClient({
    host: 'redis-server',
    port: 6379,
});
const PORT = 8080;

client.set('visits', 0);

app.get('/', (_, res) => {
    client.get('visits', (_, visits) => {
        res.send('Number of visits is ' + visits);
        client.set('visits', parseInt(visits) + 1);
    });
});

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});
